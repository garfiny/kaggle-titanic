library(ROCR)
source('./feature_engineering.R')

submission_lr <- function(model, data, threshold = 0.4) {
  pred <-predict(model, newdata = data$data, type="response")
  predictions <- list(prob = pred, labels = as.integer(pred > threshold))
  submissions <- data.frame(PassengerId = data$PassengerId, Survived = predictions$labels)
  write.csv(submissions, './lr_submission.csv', row.names = FALSE, quote=FALSE)
}

trainLogisticRegression <- function(trainData, validationData, fmla) {
  model <- glm(fmla, family = binomial(link='logit'), data = trainData)
  pred  <- predict(model, newdata = validationData, type = "response")
  as.data.frame(list(prob = pred, PassengerId = validationData$PassengerId))
}

gridSearchLRFeatures <- function() {
  data = getData(full, F)
  set.seed(1)
  formulas <- c(
    as.formula('Survived ~ Pclass + Sex + Age + Parch + Fare + familySize + titleCat + familyCat + familyId + FarePerTicket + Parent + Children + Sibling'),
    as.formula('Survived ~ Pclass + Sex + Fare + Cabin + Embarked + titleCat + Children'),
    as.formula('Survived ~ Fare + Embarked + titleCat + Parent + Children + Spouse'),
    as.formula('Survived ~ Pclass + Sex + Fare + Embarked + titleCat + Parent'),
    as.formula('Survived ~ Pclass + Sex + Age + SibSp + Parch + Fare + Embarked + titleCat + familySize')
  )

  for (fmla in formulas) {
    pred <- trainLogisticRegression(data$train$data, data$validation$data, fmla)
    evalModel(pred$prob, data$validation$labels)
  }
}

#submission_lr(model, data$test, 0.5)
